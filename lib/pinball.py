from time import sleep
from machine import Pin


class Pinball:
    delay = 0.2

    def __init__(self, pins):
        self.pins = pins

    def add_credits(self, num):
        if self.pins.is_inhibited:
            return False

        for pin in self.pin_order(num):
            sleep(self.delay)
            pin.toggle()
            sleep(self.delay)
            pin.toggle()

        return True

    def callback(self, handler=None, arg=None):
        trigger = Pin.IRQ_FALLING
        self.credit_pin(trigger, handler, arg)

    def pin_order(self, num):
        # The order to trigger the pins
        order = []
        # The number of credits left
        left = num

        keys = list(self.pins.credit_pins.keys())
        keys.sort(reverse=True)

        for key in keys:
            order += [self.pins.credit_pins[key]] * (left // key)
            left = left % key

        return order
