import sys
import urequests as requests


class Transaction:
    SUCCESS = 0
    FAIL = 1

    def __init__(self, id, credits):
        self.id = id
        self.credits = credits


class Server:
    def __init__(self, id, server):
        self.dev_id = id
        self.addr = server['addr']
        self.port = server['port'] if 'port' in server else 80
        self.cert = server['cert'] if 'cert' in server else None
        base_url = 'http{}://{}:{}/'.format('s' if self.cert else '',
                                            self.addr,
                                            self.port)
        self.transaction_url = base_url + 'get-transactions'
        self.complete_url = base_url + 'complete-transaction'

    def __str__(self):
        return str(self.addr)

    def get_transactions(self):
        try:
            response = requests.request(
                method='POST',
                url=self.transaction_url,
                json={'devID': self.dev_id},
                certfile=self.cert,
            )
        except OSError:
            print("Can't connect to {}".format(self.addr))
            return None

        if response.status_code == 200:
            try:
                json = response.json()
            except ValueError:
                print('Invalid JSON response')
                print(response.text)
            except Exception as e:
                sys.print_exception(e)
            else:
                return [Transaction(trans['id'], trans['credits']) for trans in json['transactions']]
        else:
            print('Response is not OK')
            print('Status code: {}'.format(response.status_code))
            print('Headers: {}'.format(response.headers))
            print('Body: {}'.format(response.text))

        return None

    def complete(self, transaction, status):
        try:
            response = requests.request(
                method='POST',
                url=self.complete_url,
                json={
                    'devID': self.dev_id,
                    'transID': transaction.id,
                    'status': status,
                },
                certfile=self.cert,
            )
        except Exception as e:
            print("Couldn't complete transaction")
            sys.print_exception(e)
            return

        if response.status_code != 200:
            print("Couldn't complete transaction. HTTP error code {}".format(response.status_code))
            print(response.text)
