from machine import Pin


class Pins:
    modes = {
        'IN': Pin.IN,
        'OUT': Pin.OUT,
        'DRAIN': Pin.OPEN_DRAIN,
        'NONE': None,
    }
    pull = {
        'UP': Pin.PULL_UP,
        'DOWN': Pin.PULL_DOWN,
        'NONE': None,
    }

    def __init__(self, pins_conf):
        self.credit_pins = {}
        if 'credits' in pins_conf:
            for num, conf in pins_conf['credits'].items():
                self.credit_pins[int(num)] = self.init_pin(conf)
        else:
            print('Error: No credit pins defined in the config')

        self.inhibit_pin = self.init_pin(pins_conf['inhibit']) if 'inhibit' in pins_conf else None

    @property
    def is_inhibited(self):
        return self.inhibit_pin() == 1 if self.inhibit_pin else False

    def init_pin(self, conf):
        if isinstance(conf, str):
            conf = [conf]

        mode = self.modes[conf[1] if 1 < len(conf) else 'IN']
        high = int(conf[2] if 2 < len(conf) else 0)
        low = 0 if high else 1
        pull = self.pull[conf[3]] if 3 < len(conf) else self.default_pull(mode, high)

        pin = Pin(conf[0], mode, pull)
        pin(low)
        return pin

    def default_pull(self, mode, high):
        if high:
            # If logic high
            if mode == Pin.OPEN_DRAIN:
                return None
            else:
                return Pin.PULL_DOWN
        else:
            # if logic low
            return Pin.PULL_UP
