import os
import time
import machine
import pycom
import ujson as json
from network import WLAN

from pins import Pins

pycom.heartbeat(False)
pycom.rgbled(0x000088)

known_nets = {}
with open('settings.json', 'r') as f:
    settings = json.loads(f.read())

known_nets = settings['networks']
pins = Pins(settings['pins'])

uart = machine.UART(0, 115200)
os.dupterm(uart)

if machine.reset_cause() != machine.SOFT_RESET:
    wl = WLAN(mode=WLAN.STA)

    while True:
        print("Scanning for known wifi networks")
        available_nets = wl.scan()
        nets = [net.ssid for net in available_nets]
        print('Available networks: {}'.format(', '.join(nets)))

        for net in known_nets:
            if net['ssid'] in nets:
                ssid = net['ssid']
                pwd = net['pwd']
                sec = [e.sec for e in available_nets if e.ssid == ssid][0]
                wlan_config = net['wlan_config'] if 'wlan_config' in net else None
                static_ip = net['static-ip'] if 'static-ip' in net else None
                break
        else:
            print('No known networks available')
            print('Trying again in 5 seconds...')
            time.sleep(5)
            continue

        if wlan_config:
            wl.ifconfig(config=tuple(wlan_config))
        elif static_ip:
            wl.connect(ssid, (sec, pwd), timeout=10000)
            while not wl.isconnected():
                machine.idle()
            ifconfig = list(wl.ifconfig())
            ifconfig[0] = static_ip
            wl.disconnect()
            while wl.isconnected():
                machine.idle()
            wl.ifconfig(config=tuple(ifconfig))

        wl.connect(ssid, (sec, pwd), timeout=10000)
        while not wl.isconnected():
            machine.idle()

        print("Connected to {} with IP address: {}".format(ssid, wl.ifconfig()[0]))
        break


pycom.rgbled(0x008800)
