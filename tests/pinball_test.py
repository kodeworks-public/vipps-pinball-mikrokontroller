from unittest import TestCase

from pinball import Pinball


class MockPins:
    def __init__(self, pins):
        self.credit_pins = pins
        self.is_inhibited = False


class MockPin:
    def __init__(self):
        self.num = 0
        self.state = 0

    def __call__(self, state=None):
        if state is None:
            return self.state

        if bool(self.state) != bool(state):
            self.state = state
            self.num += 1

    def toggle(self):
        self.state = 0 if self.state else 1
        self.num += 1


class PinballTest(TestCase):
    def test_pin_order(self):
        pins = {
            1: MockPin(),
            3: MockPin(),
        }
        pinball = Pinball(MockPins(pins))

        order = [pins[1]]
        self.assertEqual(pinball.pin_order(1), order)

        order = [pins[1]] * 2
        self.assertEqual(pinball.pin_order(2), order)

        order = [pins[3]]
        self.assertEqual(pinball.pin_order(3), order)

        order = [pins[3], pins[1]]
        self.assertEqual(pinball.pin_order(4), order)

        order = [pins[3]] + [pins[1]] * 2
        self.assertEqual(pinball.pin_order(5), order)

        order = [pins[3]] * 2
        self.assertEqual(pinball.pin_order(6), order)

        order = [pins[3]] * 2 + [pins[1]]
        self.assertEqual(pinball.pin_order(7), order)

        order = [pins[3]] * 2 + [pins[1]] * 2
        self.assertEqual(pinball.pin_order(8), order)

        order = [pins[3]] * 3
        self.assertEqual(pinball.pin_order(9), order)

        order = [pins[3]] * 3 + [pins[1]]
        self.assertEqual(pinball.pin_order(10), order)

    def test_add_credits(self):
        pins = MockPins({
            1: MockPin(),
            3: MockPin(),
        })
        pinball = Pinball(pins)

        pinball.add_credits(5)
        self.assertEqual(pins.credit_pins[1].num, 4)
        self.assertEqual(pins.credit_pins[3].num, 2)

        pins.is_inhibited = True
        pinball.add_credits(5)
        self.assertEqual(pins.credit_pins[1].num, 4)
        self.assertEqual(pins.credit_pins[3].num, 2)
