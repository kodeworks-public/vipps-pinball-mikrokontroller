from unittest import TestCase

from machine import Pin
from pins import Pins


class PinsTest(TestCase):
    empty_conf = {
        'credits': {
            '1': 'P23'
        }
    }

    default_conf = {
        'credits': {
            '1': 'P23',
            '3': 'P22',
        },
        'inhibit': ['P21', 'IN'],
    }

    def test_default_pull(self):
        pins = Pins(self.empty_conf)

        self.assertEqual(pins.default_pull(Pin.IN, 1), Pin.PULL_DOWN)
        self.assertEqual(pins.default_pull(Pin.IN, 0), Pin.PULL_UP)

        self.assertEqual(pins.default_pull(Pin.OUT, 1), Pin.PULL_DOWN)
        self.assertEqual(pins.default_pull(Pin.OUT, 0), Pin.PULL_UP)

        self.assertIsNone(pins.default_pull(Pin.OPEN_DRAIN, 1))
        self.assertEqual(pins.default_pull(Pin.OPEN_DRAIN, 0), Pin.PULL_UP)

    def test_init_pin(self):
        pins = Pins(self.empty_conf)

        self.assertEqual(pins.init_pin(['P23']), Pin('P23', mode=Pin.IN, pull=Pin.PULL_UP))
        self.assertEqual(pins.init_pin(['P23', 'IN']), Pin('P23', mode=Pin.IN, pull=Pin.PULL_UP))
        self.assertEqual(pins.init_pin(['P23', 'OUT']), Pin('P23', mode=Pin.OUT, pull=Pin.PULL_UP))
        self.assertEqual(pins.init_pin(['P23', 'DRAIN']), Pin('P23', mode=Pin.OPEN_DRAIN, pull=Pin.PULL_UP))

        self.assertEqual(pins.init_pin(['P23', 'IN', 1]), Pin('P23', mode=Pin.IN, pull=Pin.PULL_DOWN))
        self.assertEqual(pins.init_pin(['P23', 'OUT', 1]), Pin('P23', mode=Pin.OUT, pull=Pin.PULL_DOWN))
        self.assertEqual(pins.init_pin(['P23', 'DRAIN', 1]), Pin('P23', mode=Pin.OPEN_DRAIN, pull=None))

        pin = pins.init_pin(['P23', 'OUT', 0])
        self.assertEqual(pin(), 1)

        pin = pins.init_pin(['P23', 'OUT', 1])
        self.assertEqual(pin(), 0)

    def test_create(self):
        pins = Pins(self.default_conf)
        self.assertEqual(len(pins.credit_pins), 2)
        self.assertEqual(pins.credit_pins[1], Pin('P23', mode=Pin.OPEN_DRAIN, pull=Pin.PULL_UP))
        self.assertEqual(pins.credit_pins[3], Pin('P22', mode=Pin.OPEN_DRAIN, pull=Pin.PULL_UP))
        self.assertEqual(pins.inhibit_pin, Pin('P21', mode=Pin.IN, pull=Pin.PULL_UP))

    def test_is_inhibited(self):
        pins = Pins(self.default_conf)

        state = 1
        pins.inhibit_pin = lambda: state

        self.assertTrue(pins.is_inhibited)

        state = 0
        self.assertFalse(pins.is_inhibited)
