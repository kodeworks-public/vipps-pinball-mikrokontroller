import unittest
import json

from pins import Pins
from pinball import Pinball
from .pinball_test import *
from .pins_test import *


def run():
    unittest.main('tests')


def pinball():
    with open('settings.json', 'r') as f:
        settings = json.loads(f.read())

    pins = Pins(settings['pins'])
    return Pinball(pins)


def add_credits(num):
    pinball().add_credits(num)
