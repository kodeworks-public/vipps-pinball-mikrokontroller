import pycom
import json
from network import WLAN
from machine import reset
from time import sleep

from server import Server, Transaction
from pins import Pins
from pinball import Pinball


def main():
    pycom.heartbeat(False)

    with open('settings.json', 'r') as f:
        settings = json.loads(f.read())

    pins = Pins(settings['pins'])
    pinball = Pinball(pins)
    servers = [Server(settings['id'], server) for server in settings['servers']]
    wlan = WLAN(mode=WLAN.STA)
    connected = False

    while True:
        pycom.rgbled(0x111111)
        sleep(1)
        pycom.rgbled(0x171717)

        if not wlan.isconnected():
            print('Network disconnected. Restarting...')
            reset()

        for server in servers:
            transactions = server.get_transactions()
            if isinstance(transactions, list):
                if not connected:
                    print('Connected to server')
                    connected = True

                for transaction in transactions:
                    if pinball.add_credits(transaction.credits):
                        status = Transaction.SUCCESS
                        print('Added {} credits'.format(transaction.credits))
                    else:
                        status = Transaction.FAIL
                        print('The machine is not accepting credits')

                    server.complete(transaction, status)
                break
        else:
            print('Could not connect to servers')
            print('Trying again in 5 seconds')
            pycom.rgbled(0x550000)
            connected = False
            sleep(5)


main()
